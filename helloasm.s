// Text file: helloasm.s
// Compile and link:
// as -o helloasm.o helloasm.s && ld -s -o helloasm helloasm.o
// Binary form: hexdump -C helloasm
// Disassembler: objdump -D helloasm

.section .text			# code section
	.globl	_start		# entry point - information for the linker
_start: movl	$4,%eax		# 4 means "write" for interrupt 0x80
	xorl	%ebx,%ebx
	incl	%ebx		# 1 means stdout
	movl	$string,%ecx	# string address
	movl	$length,%edx	# string length
	int	$0x80		# call "write", parameters %eax, %ebx, %ecx, %edx (, %esi, %edi)
	xorl	%eax,%eax
	incl	%eax		# 1 means "exit"
	xorl	%ebx,%ebx	# 0 is the return code
	int	$0x80 		# call "exit", parameters %eax, %ebx
	hlt			# this point is never reached (hlt just in case)
.section .data			# data section
string:	.string "Hello, Assembler world!\012"
length = .-strin		# length is difference of two addresses
// end of file

